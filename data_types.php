<?php

    // boolean start here

    $decision = true;
    if($decision)
    {
        echo "The decision is true<br>";
    }

    $decision = false;
    if($decision)
    {
        echo "The decision is false<br>";
    }

    //boolean end here

    // integer & float start here

    $value1 = 100;      //integer
    $value2 = 55.36;    //float

    // integer & float end here

    //string start here

    $myString1 = 'abcd1234# $value1';
    $myString2 = "abcd1234# $value1";
    echo $myString1 . "<br>";
    echo $myString2 . "<br>";

    $heredocString =<<<BITM
        heredoc line1 $value1 <br>
        heredoc line2 $value1 <br>
        heredoc line3 $value1 <br>
        heredoc line4 $value1 <br>
BITM;

    echo $heredocString;

    $nowdocString =<<<'BITM'
        nowdoc line1 $value1 <br>
        nowdoc line2 $value1 <br>
        nowdoc line3 $value1 <br>
        nowdoc line4 $value1 <br>
BITM;
    echo $nowdocString;

    //string end here

    //array start here

    $indexedArray = array(1,2,3,4,5,6,7);
    print_r($indexedArray);

    $indexedArray = array("Toyota","BMW","Jaguar",3,5,7,"Nissan","Ford");
    print_r($indexedArray);

    $ageArray = array("Rahim"=>23,"Saju"=>26,"Mohiuddin"=>30);
    print_r($ageArray);
    $ageArray['Saju'] = 32;
    echo($ageArray['Mohiuddin']);

    echo "<pre>";
    print_r($ageArray);
    echo "</pre>";
    //array end here


?>