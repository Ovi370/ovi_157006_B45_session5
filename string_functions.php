<?php

    $str="Is your name O'Reilly?";
    echo addslashes($str);
    echo "<br>";

    $str="html css php my sql";
    $arr=explode(" ",$str);
    echo "<pre>";
    print_r($arr);
    echo "<pre>";

    $st=array("html","css","php","mysql");
    $imp=implode(",",$st);
    echo $imp."<br>";
    var_dump($imp);

    $str="<br> is a html tag";
    echo "htmlentities: ";
    echo htmlentities($str);
    echo "<br>";

    $str="Hello World!";
    echo $str."<br>";
    echo "trim: ";
    echo trim($str,"Hed!");
    echo "<br>";

    $str="Hello World!";
    echo $str."<br>";
    echo "ltrim: ";
    echo ltrim($str,"Hel");
    echo "<br>";

    $str="Hello World!";
    echo $str."<br>";
    echo "rtrim: ";
    echo rtrim($str,"d!");
    echo "<br>";

    echo nl2br("one line.\nAnother line");
    echo "<br>";

    $str= "Hello World";
    echo "srt_pad: ";
    echo str_pad($str,25,"!");
    echo "<br>";

    echo "str_repeat";
    echo str_repeat("Hi!",10);
    echo "<br>";

    echo "str_replace: ";
    echo str_replace("World","Ovi","Hello World!");
    echo "<br>";

    echo "srt_split: ";
    print_r(str_split("Hello"));
    echo "<br>";

    echo "strlen: ";
    echo strlen("Hello World!");
    echo "<br>";

    echo "strtolower: ";
    echo strtolower("Hello World");
    echo "<br>";

    echo "strtoupperr: ";
    echo strtoupper("Hello World");
    echo "<br>";

    echo "substr_compare: ";
    echo substr_compare("Hello World","Hello World",0);
    echo "<br>";
    echo substr_compare("Hello World","Wo",0);
    echo "<br>";
    echo substr_compare("Hello Worl","Hello World",0);
    echo "<br>";

    echo "substr_count: ";
    echo substr_count("Hello World! The world is round. World is beautiful","World");
    echo "<br>";

    $str="Hello World";
    echo "substr_replace: ";
    echo substr_replace("Hello","World",4);
    echo "<br>";

    $str="ovi chowdhury";
    echo "Upper Case First: ";
    echo ucfirst($str);
    echo "<br>";

    $str="ovi chowdhury";
    echo "Upper Case All Words: ";
    echo ucwords($str);
    echo "<br>";


?>