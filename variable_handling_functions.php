<?php

    //floatval
    $var = 'The122.34343The';
    $float_value_of_var = floatval($var);
    echo $float_value_of_var; // 122.34343

    //empty
    echo "<br>";
    $var="";
    if(empty($var)){
        echo '$var is empty';
    }
    else{
        echo '$var is not empty';
    }

    echo "<br>";
    $var= array(1,2,3,4,5,6,7,8);
    if(is_array($var)){
        echo '$var is an array';
    }
    else{
        echo '$var is not an array';
    }

    echo "<br>";
    $strSerialize = serialize($var);
    echo $strSerialize;

    echo "<br>";
    $newVar = unserialize($strSerialize);
    print_r($newVar);

    echo "<br>";
    var_dump($var);

    echo "<br>";
    var_export($var);

   /* echo "<br>";
    echo get_type($var);*/



   /* unset($var);
    print_r($var);*/

    echo "<br>";
    $a = false;
    $b = 0;

    // Since $a is a boolean, it will return true
    if (is_bool($a) === true) {
        echo "Yes, this is a boolean";
        echo "<br>";
    }

    // Since $b is not a boolean, it will return false
    if (is_bool($b) === false) {
        echo "No, this is not a boolean";
        echo "<br>";
    }

    echo "<br>";
    $myValue = 0;
    //boolval($myValue);
    var_dump(boolval($myValue));




?>